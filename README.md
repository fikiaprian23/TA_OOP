**Tugas Besar: Clone WhatsApp**

Nama: Fiki Aprian

NIM: 1217050058

Kelas: Praktikum PBO A

**Deskrispi Aplikasi**

WhatsApp adalah aplikasi pesan instan yang memungkinkan pengguna untuk mengirim pesan teks, suara, dan video secara gratis melalui internet. Selain itu, pengguna dapat melakukan panggilan suara dan video, serta mengirim file seperti dokumen, gambar, dan video. Aplikasi ini dapat diakses melalui platform iOS dan Android serta web.

**Teknologi yang Digunakan**
- Bahasa Pemrograman: Dart
- Framework: Flutter
- Database: Firebase

**USE CASE PRIORITY**
| Use Case                     | Priority | Status |
|------------------------------|----------|--------|
| Registrasi          | Tinggi   | Selesai
| Liat kontak          | Tinggi   | Selesai |
| Mengirim Pesan Teks          | Tinggi   | Selesai |
| Mengirim Pesan Suara         | Tinggi   | Selesai |
| Mengirim Pesan Video         | Tinggi   | Selesai |
| Menerima Pesan               | Tinggi   | Selesai |
| Menyimpan Pesan              | Tinggi   | Selesai |
| Membuat Grup                 | Tinggi   | Selesai
| Mengirim Pesan di Grup       | Tinggi   | Selesai |
|  Panggilan Suara     | Tinggi   | **Belum** |
|  Panggilan Video     | Tinggi   | **Ongoing** |
| Mengirim Berkas              | Tinggi   | **Belum** |
| Menyimpan Riwayat Panggilan  | Sedang   | **Baru UI** |
| Mengatur Notifikasi          | Sedang   | **Belum** |
| Buat Status Teman         | Sedang   | **Baru Ui** |
| Melihat Status Teman         | Sedang   | **Baru Ui** |
| Liat Profil teman              | Sedang   | Selesai |
| Ubah profile              | Sedang   | **Belum** |

